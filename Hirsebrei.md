# Süßer Hirsebrei

## Zutaten

| Menge | Zutat |
| ----------- | ----------- |
| 1/4 Tasse | Hirse (schnellkochend, aus dem Reformhaus) |
| 1 Schote | Kardamom |
| 1 Prise | Salz |
| 1 Tl | Fohsamenschalen |
| 1 El | Walnussmus oder ein paar Walnüsse |
| 1/2 - 1 Tl | Honig oder Agavendicksaft |
| 1 | Banane oder Birne |

## Zubereitung

- Die Kardamomschote öffnen und die schwarzen Samen rauspulen.
- Die Hirse mit ca. 3-fachen Menge Wasser einer Prise Salz und den Kardamom-Kernen für 10 Minuten kochen.
- Walnüsse kleinbrechen, in einer kleinen Pfanne rösten. (Etwas Honig dazu geben, damit es karamellisiert.)
- Walnussmus untermischen, wenn vorhanden
- Obst dazu schnibbeln und mit etwas Honig oder Agavenndicksaft o.ä. abschmecken. Doch obacht, reifes Obst liefert sehr viel Zucker, lieber weniger süßen!
- Flohsamenschalen dazugeben und ggf. mit etwas Wasser, es darf ruhig ein klein wenig suppig sein. Wenn zu viel Flüssigkeit übrig ist, binden die Flohsamenschalen sehr viel davon, bei Bedarf kann man einen halben Tl mehr nehmen.
 - Vor allem bei nachträglicher Wasserzugabe ganz kurz - wirkich nur ganz kurz - aufkochen.
- geröstete Walnüsse dazugeben
- Für 10 Minuten bei geschlossenem Deckel ziehen lassen.

