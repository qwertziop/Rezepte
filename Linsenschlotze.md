# Linsenschlotze

## Zutaten

| Menge | Zutat |
| ----------- | ----------- |
| 1 Tasse | Linsen, rot, geschält |
| 1 | Zwiebel |
| ~80g | Magarine |
| 1 el | Tomatenmark |
|  | Salz & Pfeffer |

## Zubereitung

- 1 Tasse geschälte rote Linsen nach Anweisung kochen
- 1 nicht zu kleine Zwiebel abziehen und fein würfeln
- die noch heißen Linsen mit 1 ordentliches Stück Magarine (~80g, im Zweifel eher mehr) und den rohen Zwiebeln und Tomatenmark vermengen
- mit Salz & Pfeffer abschmecken
- 1-2 Std. ziehen lassen

